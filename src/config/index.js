//NODE_ENV = production || development || other
let env = process.env.NODE_ENV;

if (!env) {
	env = 'development';
}

const CONFIG = require('./' + env + '.json');

module.exports = {
    CONFIG: CONFIG,
    accountSid: process.env.ACCOUNT_ID,
    serviceId:  process.env.SERVICE_ID,
    authToken:  process.env.AUTH_TOKEN,
    phoneNumber: process.env.PHONE
};

