const jwt = require('jsonwebtoken');
const { CONFIG } = require('../config');
const bcrypt = require('bcrypt');
const verify = require('../2fa');
const knex = require('knex')(CONFIG.DB);


const login = ('/login', async (username, password) => {
  const user = await knex('private.auth').where({
    phone_number: username
  }).first();

  console.log(user);
  if (user) {
    const checkPwd = await bcrypt.compare( password, user.password );

    console.log(password);
    const genPassword = await bcrypt.hash(password, 10);
    console.log(user.password, 'Gen: ', genPassword);

    console.log(checkPwd);


    if ( checkPwd ) {
      const payload = {
        id: user.id,
        phone_number: user.phone_number,
        is_admin: user.is_admin,
        party_id: user.party_id,
        date_added: user.date_added
      };

      const superSecret = CONFIG.JWT.secret + user.id + user.date_added + user.phone_number;
      const token = jwt.sign(payload, superSecret, { expiresIn: '1h' });
      console.log(token);
      return {
        status: 200,
        token,
        message: "Authentication Successful!"
      }
    } else {
      return {
        status: 201,
        message: 'Authentication failed'
      }
    }
  } else {

    return {
      status: 201,
      message: 'User not found'
    }
  }
});


const signup = ('/signup', async (user) => {
  console.log(user);
  try {
    const party = {
      name: user.name,
      phone_number: user.phone_number,
      email:user.email
    }
    
    const [insertedParty] = await knex('private.party')
      .insert(party)
      .returning("*")

    console.log(insertedParty)
    const genPassword = await bcrypt.hash(user.password, 10)
    await knex('private.auth').insert({
      phone_number: user.phone_number,
      password: genPassword,
      party_id: insertedParty.id
    })

    await knex('private.account').insert({
      name: user.name,
      code: user.phone_number,
      party_id: insertedParty.id
    })
    
    
    return {
      status: 200,
      message: "Sign up successful"
    }

  } catch(err){
    console.log(err);
    return {
      status: 201,
      message: "An error occurred while signing up the user",
    }
  }
});


const sendVerification = ('/send-token', async(no) => {
    const resp = await verify.sendVerification(no);
    if(resp) {
        return {
            status: 200,
            message: "A verification code was sent to your phone"
        };

    } else {
        return {
            status: 500,
            message: "An error occurred"
        };

    }

});


const checkVerification = ('/verify-token', async(token, no) => {

    const resp = await verify.verifyToken(token, no);

    if( resp === 'approved' ) {
        return {
            status: 200,
            message: "Verification successful"
        };

    } else {
        return{
            status: 500,
            message: "Verification failed!"
        };

    }

});


module.exports = {
  login,
  signup,
  sendVerification,
  checkVerification
};
