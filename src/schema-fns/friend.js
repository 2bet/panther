const { CONFIG } = require('../config')
const jwtVerify = require('../middleware/jwt-verify')

const knex = require('knex')(CONFIG.DB)


const addFriend = ('/friend', jwtVerify, async (ctx, next)=> {
  try {
    const friend = ctx.request.body;
    const [party] = await knex('private.party').where({phone_number: friend.phone_number})
    console.log(party)
    party.is_admin = ctx.state.user.is_admin
    ctx.body = party
    await knex('private.party_relationship').insert({
      from_party: ctx.state.user.party_id,
      to_party: party.id
    })

  } catch(e) {
    console.log(e)
    ctx.body = {
      data: {
        status: 201,
        message: "Error"
      }
    }
  }
  await next()
})

const getFriends = ('/friends', jwtVerify, async (ctx, next)=> {
  const users = await knex('private.party').select("*")
  .whereIn('id', function() { 
    this.select('to_party').from('private.party_relationship').where({
      from_party:  ctx.state.user.party_id,
      relationship_type: 'Friend'
    });
  })
  console.log(users)
  ctx.body = users
  await next()
})

const getFriendById = ('/friend/:id', jwtVerify, async (ctx, next)=> {
  const {id} = ctx.params
  const [user] = await knex('private.party').select("*")
  .whereIn('id', function() { 
    this.select('to_party').from('private.party_relationship').where({
      to_party:  id,
      relationship_type: 'Friend'
    });
  })
  ctx.body = user
  await next()
})

module.exports = {
  addFriend,
  getFriends,
  getFriendById
}
