import jwt from 'jsonwebtoken'
const { CONFIG } = require('../config');
import jwtVerify from '../middleware/jwt-verify'

const knex = require('knex')(CONFIG.DB)

const currentUser = ('/current-user', jwtVerify, async (ctx, next)=> {
  try {
    const [party] = await knex('private.party').where({id: ctx.state.user.party_id})
    console.log(party)
    party.is_admin = ctx.state.user.is_admin
    ctx.body = party
  } catch(e) {
    console.log(e)
    ctx.body = {
      data: {
        status: 201,
        message: "Error"
      }
    }
  }
  await next()
});

const getUsers = ('/users', jwtVerify, async (ctx, next)=> {
  if(ctx.state.user.is_admin){
    const users = await knex('private.party').select("*")
    console.log(users)
    ctx.body = users
    await next()
  }else {
    ctx.throw(401)
  }
})

const getUserById = ('/user/:id', jwtVerify, async (ctx, next)=> {
  if(ctx.state.user.is_admin){
    const {id} = ctx.params
    const [user] = await knex('private.party').where({id})
    ctx.body = user
    await next()
  }else {
    ctx.throw(401)
  }
})

module.exports = {
  currentUser,
  getUserById,
  getUsers,
}
