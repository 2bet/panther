const auth = require('./auth')
const friend = require('./friend')
const user = require('./friend')
const payments = require('./payments')
// console.log("auth: ", auth)

module.exports = {
  auth,
  friend,
  user,
  payments,
};
