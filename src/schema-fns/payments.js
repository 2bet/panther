const request = require('request-promise');


const deposit = ('/login', async (phone, amount) => {
  console.log(phone, amount);

  const opts = {
    url: 'https://mpesa.2bet.co.ke/topup/mpesa',
    form: {
      phone,
      amount,
    }
  };
  
  return request.post(opts)
});


const withdraw = ('/login', async (phone, amount) => {
  console.log(phone, amount);

  const opts = {
    url: 'https://mpesa.2bet.co.ke/withdraw/mpesa',
      form: {
        phone,
        amount
      }
  };

  return request.post(opts);

});


module.exports = {
  deposit,
  withdraw
};
