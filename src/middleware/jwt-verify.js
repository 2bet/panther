const jwt = require('jsonwebtoken')
const { CONFIG } = require('../config')
// const dateFns = require('date-fns)'

const knex = require('knex')(CONFIG.DB)

async function verify(ctx, next){
    if (!ctx.header || !ctx.header.authorization) {
      ctx.throw(401,'Authentication Error')
    }
    try {
      const parts = ctx.request.header.authorization.split(' ')
      if (parts.length === 2) {
          const scheme = parts[0]
          const credentials = parts[1]

          if (/^Bearer$/i.test(scheme)) {
            
            const cerf = jwt.decode(credentials)
            
            const [user] = await knex('private.auth').where({ id: cerf.id})
            const superSecret = CONFIG.JWT.secret + user.id + user.date_added + user.phone_number + ctx.request.header.host
            try {
              jwt.verify(credentials, superSecret)
              delete user.password
              ctx.state = { user: user }
            } catch (err) {
              ctx.throw(401,'Authentication Error')
            }
            
          }
      } else {
          ctx.throw(401, 'Bad Authorization header format. Format is "Authorization: Bearer <token>"')
      }
    } catch(e) {
      ctx.throw(401)
    }
    await next()
}

module.exports.verify = verify
