const { accountSid, authToken, serviceId } = require('../config');

const Twilio = require('twilio');
const client = new Twilio(accountSid, authToken);


const sendVerification = async (no) => {
    /**
     * to ensure the user has the device to withdraw to,
     * send a token to the registered number
     */
    const contact = formatNumber(no);
    let verificationRequest;
    try {
        verificationRequest = await client.verify.services(serviceId)
            .verifications
            .create({
                to: contact,
                channel: 'sms'

            });
        return verificationRequest.sid;
    } catch (e) {
        console.log(e);
        return {
            status: 500,
            message: "An error occured!"
        }

    }


};


const verifyToken = async (token, no) => {
    /**
     * verify the token sent to user
     * @return {String} pending || approved || denied
     */
    const contact = formatNumber(no);
    let verificationRequest;
    try {
        verificationRequest = await client.verify.services(serviceId)
            .verificationChecks
            .create({
                to: contact,
                code: token

            });

    } catch (e) {
        console.log(e);
        return;
    }

    return verificationRequest.status;

};


function formatNumber(contact) {
    if (contact.startsWith("0")) {
        return "+254" + contact.substr(1);

    } else if(contact.startsWith("254")) {
        return "+" + contact;

    }

    return contact;

}

module.exports = {
    sendVerification,
    verifyToken
};
