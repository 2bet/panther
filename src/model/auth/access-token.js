const moment = require('moment');
const {CONFIG} = require('../../config');
const knex = require('knex')(CONFIG.DB);
const crypto = require('crypto');

const TABLE = 'private.oauth_access_token'

module.exports.getToken = accessToken => accessToken.token

module.exports.create = (userId, clientId, scope, ttl, cb) => {
  const token = crypto.randomBytes(64).toString('hex')
  const obj = { token: token, userId: userId, clientId: clientId, scope: scope, ttl: new Date().getTime() + ttl * 1000 }

  try {
    knex(TABLE).insert(obj)
  } catch (error) {
    console.log(error)
  }

  cb(null, token)
}

module.exports.fetchByToken = async (token, cb) =>
  knex(TABLE).select().where({ token }).first()
    .asCallback(cb)

module.exports.checkTTL = accessToken => (accessToken.ttl > new Date().getTime())

module.exports.getTTL = (accessToken, cb) => {
  const ttl = moment(accessToken.ttl).diff(new Date(), 'seconds')
  return cb(null, ttl > 0 ? ttl : 0)
}

module.exports.fetchByUserIdClientId = (userId, clientId, cb) => {
  knex(TABLE)
    .select()
    .where({
      user_id: userId,
      client_id: clientId,
    })
    .orWhere('ttl', '>', new Date().getTime())
    .orderBy('ttl', 'desc')
    .limit(1)
    .first()
    .asCallback(cb)
}
