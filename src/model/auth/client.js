const {CONFIG} = require('../../config');
const knex = require('knex')(CONFIG.DB);

var TABLE = 'private.oauth_client';

module.exports.getId = client => client.clientId

module.exports.getRedirectUri = client => client.redirectUri

module.exports.checkRedirectUri = (client, redirectUri) =>
  (redirectUri.indexOf(getRedirectUri(client)) === 0 &&
    redirectUri.replace(getRedirectUri(client), '').indexOf('#') === -1)

module.exports.fetchById = async (clientId, cb) => {
  console.log("Finding Client..")
  console.log(await knex(TABLE).select().where({ clientId }).toString())
  knex(TABLE).select().where({ clientId }).first()
    .asCallback((err, client) =>{
      console.log(client)
      if(err) cb(err);
      else cb(null, client)
    })
}

module.exports.checkSecret = (client, secret, cb) =>
  cb(null, client.clientSecret === secret)

module.exports.checkGrantType = (client, grantType) => {
  console.log(grantType)
  return client.authorizedGrantTypes === grantType
}

/**
 * Checks scope permission for the client
 * Default: do not check it, return empty array
 * Function arguments MAY be different
 *
 * @param client {Object} Client object
 * @param scope {String} Scope string (space delimited) passed via parameters
 * @return {Array|boolean} Return checked scope array or false
 */
module.exports.checkScope = function (client, scope) {
  /**
   * For example:
   * scope.forEach(function(item) {
   *   if (scope.indexOf(item) == -1) return false;
   * });
   * return scope;
   */
  return [];
};

/**
 * Transforms scope body parameter to scope array
 *
 * @param scope
 */
module.exports.transformScope = function (scope) {
  if (!scope) return [];
  return scope.split(' ');
};