const {CONFIG} = require('../../config');
const knex = require('knex')(CONFIG.DB);

const TABLE = 'oauth_code'

module.exports.create = (userId, clientId, scope, ttl, cb) =>  {
  const code = crypto.randomBytes(32).toString('hex')
  const obj = { code, userId, clientId, scope, ttl: new Date().getTime() + ttl * 1000}

  knex(TABLE).insert(obj).returning('*')
  .asCallback(cb)
}

module.exports.fetchByCode = (code, cb) => {
  knex(TABLE).select().where({ code })
  .limit(1)
  .asCallback(cb)
}

module.exports.getUserId = code => code.userId

module.exports.getClientId = code => code.clientId

module.exports.getScope = code => code.scope

module.exports.checkTtl = code => (code.ttl > new Date().getTime())

module.exports.removeByCode = (code, cb) => {
  knex(TABLE).delete().where({ code: code })
  .asCallback(cb)
}
