const {CONFIG} = require('../../config');
const knex = require('knex')(CONFIG.DB);
const crypto = require('crypto');

const TABLE = 'private.oauth_refresh_token';


module.exports.getUserId = refreshToken => refreshToken.userId

module.exports.getClientId = refreshToken => refreshToken.clientId

module.exports.getScope = refreshToken => refreshToken.scope

module.exports.fetchByToken = (token, cb) => {
  knex(TABLE).select().where({ token }).first().asCallback(cb)
}

module.exports.removeByUserIdClientId = (userId, clientId, cb) => {
  knex(TABLE)
  .del()
  .where({
    userId: userId,
    clientId: clientId
  })
  .then(cb)
}

module.exports.create = (userId, clientId, scope, cb) => {
  const token = crypto.randomBytes(64).toString('hex')
  const obj = { token, userId, clientId, scope }
  try {
    knex(TABLE).insert(obj).returning('*')
    .asCallback(cb)    
  } catch (error) {
    console.log(error)
  }
}