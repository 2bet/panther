module.exports = {
  accessToken:    require('./access-token.js'),
  client:         require('./client.js'),
  code:           require('./code.js'),
  refreshToken:   require('./refresh-token.js'),
  user:           require('./user.js')
};