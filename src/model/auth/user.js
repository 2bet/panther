const {CONFIG} = require('../../config');
const knex = require('knex')(CONFIG.DB);

const TABLE = 'private.user'

module.exports.getId = user => user.id

module.exports.fetchById = function (id, cb) {
  knex(TABLE).select().where({ id }).first()
  .asCallback(cb)
}

module.exports.fetchByUsername = (username, cb) => {
  knex(TABLE).select().where({ username }).first()
  .asCallback(cb)
}

module.exports.checkPassword = (user, password, cb) => {
  (user.password == password) ? cb(null, true): cb(null, false)
}

module.exports.fetchFromRequest = req => req.session.user