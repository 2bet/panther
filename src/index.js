require('dotenv').config();
const express = require("express");
const graphqlHTTP = require('express-graphql');
const app = express();
const port = 3038;
const { schema, graphql } = require("./schema");


app.post('/', graphqlHTTP({
  schema: schema,
  graphiql: false,
}));

app.get('/', graphqlHTTP({
  schema: schema,
  graphiql: true,
}));

app.listen(port, () => console.log(`Panther is listening on port ${port}!`))
