const {
  graphql,
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLList,
  GraphQLNonNull
} = require("graphql");


const { CONFIG } = require('./config');

const knex = require('knex')(CONFIG.DB);

const fns = require('./schema-fns');

const user = new GraphQLObjectType({
  name: "User",
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    phone_number: { type: new GraphQLNonNull(GraphQLInt) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  }
});

const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Panther",
    fields: {
      hello: {
        type: GraphQLString,
        resolve() {
          return "Hello, Panther server is up and running."
        }
      },
    }
  }),
  mutation: new GraphQLObjectType({
    name: "MutantPanther",
    fields: {
      login: {
        type: new GraphQLObjectType({
          name: "authenticationPayload",
          fields: {
            status: { type: GraphQLInt },
            token: { type: GraphQLString },
            message: { type: GraphQLString },
          }
        }),
        args: {
          username: { type: new GraphQLNonNull(GraphQLString) },
          password: { type: new GraphQLNonNull(GraphQLString) }
        },
        resolve: async (_, { username, password }) => {
          const res = await fns.auth.login(username, password);
          console.log(res);
          return res
        }
      },
      signup: {
        type: new GraphQLObjectType({
          name: "signup",
          fields: {
            status: { type: GraphQLInt },
            message: { type: GraphQLString }
          }

        }),
        args: {
          name: { type: new GraphQLNonNull(GraphQLString) },
          email: { type: new GraphQLNonNull(GraphQLString) },
          phone_number: { type: new GraphQLNonNull(GraphQLString) },
          password: { type: new GraphQLNonNull(GraphQLString) }

        },
        resolve: async (_, user) => {
          const res = await fns.auth.signup(user);
          console.log(res);
          return res

        }

      },
      sendVerification: {
          type: new GraphQLObjectType({
              name: "sendVerification",
              fields: {
                  status: { type: GraphQLInt },
                  message: { type: GraphQLString }

              }

          }),
          args: { phone: { type: new GraphQLNonNull(GraphQLString) } },
          resolve: async(_, { phone }) => {
              const res = await fns.auth.sendVerification(phone);
              console.log(res);
              return res;

          }

      },
      checkVerification: {
         type: new GraphQLObjectType({
             name: "checkVerification",
             fields: {
                 status: { type: GraphQLInt },
                 message: { type: GraphQLString }

             }

         }),
          args: {
              phone: { type: new GraphQLNonNull(GraphQLString) },
              token: { type: new GraphQLNonNull(GraphQLString) }

          },
          resolve: async(_, { phone, token }) => {
             const res = await fns.auth.checkVerification(token, phone);
             console.log(res);
             return res;

          }

      },
      deposit: {
        type: new GraphQLObjectType({
          name: 'deposit',
          fields: {
            statusCode: { type: GraphQLInt },
            statusName: { type: GraphQLString }
          }

        }),
          args: {
            phone: { type: GraphQLString },
            amount: { type: GraphQLInt }

        },
        resolve: async(_, { phone, amount }) => {
          const res = await fns.payments.deposit(phone, amount);
          console.log(res);
          const { statuscode, statusname} = JSON.parse(res);
          if(statuscode === 0){
            return {
              statusCode: statuscode,
              statusName: statusname,
            }
          }else {
            return {
              statusCode: 1,
              statusName: 'Error'
            }
          }
        }
      },
      withdraw: {
        type: new GraphQLObjectType({
          name: 'withdraw',
          fields: {
            statusCode: { type: GraphQLInt },
            statusName: { type: GraphQLString }
          }
        }),
        args: {
          phone: { type: GraphQLString },
          amount: { type: GraphQLInt }
        },
        resolve: async(_, {phone, amount}) => {
          const res = await fns.payments.withdraw(phone, amount);
          console.log(res);
          const {statuscode, statusname} = JSON.parse(res);
          if(statuscode === 0) {
            return {
              statusCode: statuscode,
              statusName: statusname
            }

          } else {
            return {
              statusCode: 1,
              statusName: 'Error'
            }

          }

        }

      },
      balance: {
        type: new GraphQLObjectType({
          name: "balance",
          fields: {
            balance: { type: GraphQLFloat },
          }
        }),
        args: {
          phone_number: { type: new GraphQLNonNull(GraphQLString) }
        },
        resolve: async (_, { phone_number}) => {
          const credit = await knex('private.posting')
            .sum('credit')
            .whereIn(['account_id'], knex.select('id')
              .from('private.account')
              .where({
                code: phone_number
              })
            )
            .first();
          const debit = await knex('private.posting')
            .sum('debit')
            .whereIn(['account_id'], knex.select('id')
              .from('private.account')
              .where({
                code: phone_number
              })
            )
            .first();

          return {
            balance: credit.sum - debit.sum
          }
        }
      }
    }
  })
});


module.exports = {
  schema,
  graphql
};
